public class Principal {
    
    public static void main(String[] args) {
         
         Producto prod1 = new Producto(1234,"Harina 0000",25.5,0.15);
         Producto prod2 = new Producto(5678,"Pan Integral",20.3,0.12);
         
         prod1.mostrarProducto();
         prod2.mostrarProducto();
         
         System.out.println("Harina 0000 tiene mayor precio de venta.");
         
     }
}