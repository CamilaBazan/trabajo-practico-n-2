public class Producto {
    
    private int codproducto;
    private String nomproducto;
    private double preciocosto;
    private double ganancia;
    private double iva=0.21;
    
    public Producto(int codproducto, String nomproducto, double preciocosto, double ganancia){
        
        this.codproducto=codproducto;
        this.nomproducto=nomproducto;
        this.preciocosto=preciocosto;
        this.ganancia=ganancia;
    }

    public int getCodproducto() {
        return codproducto;
    }

    public void setCodproducto(int codproducto) {
        this.codproducto = codproducto;
    }

    public String getNomproducto() {
        return nomproducto;
    }

    public void setNomproducto(String nomproducto) {
        this.nomproducto = nomproducto;
    }

    public double getPreciocosto() {
        return preciocosto;
    }

    public void setPreciocosto(double preciocosto) {
        this.preciocosto = preciocosto;
    }

    public double getGanancia() {
        return ganancia;
    }

    public void setGanancia(double ganancia) {
        this.ganancia = ganancia;
    }

    public double getIva() {
        return iva;
    }

    public void setIva(double iva) {
        this.iva = iva;
    }

    public double calcularPrecio(double preciocosto, double ganancia, double iva){
        
        double precioventa=0;
        
        precioventa=(((preciocosto*ganancia)+preciocosto)*iva)+preciocosto;
        
        return precioventa;
    }
    
    public void mostrarProducto(){
        
        System.out.println("Codigo: "+codproducto);
        System.out.println("Producto: "+nomproducto);
        System.out.println("Precio de Costo: "+preciocosto);
        System.out.println("Ganancia: "+ganancia);
        System.out.println("IVA: "+iva);
        System.out.println("Precio de Venta: "+calcularPrecio(preciocosto,ganancia,iva));
        System.out.println("\n");
    }
}
