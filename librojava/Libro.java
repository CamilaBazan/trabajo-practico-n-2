public class Libro {
    
    private int ISBN;
    private String titulo;
    private String autor;
    private int numpaginas;
    
    
    public Libro(int ISBN,String titulo, String autor, int numpaginas){
        
        this.ISBN=ISBN;
        this.titulo=titulo;
        this.autor=autor;
        this.numpaginas=numpaginas;
        
    }

    public int getISBN() {
        return ISBN;
    }

    public void setISBN(int ISBN) {
        this.ISBN = ISBN;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getNumpaginas() {
        return numpaginas;
    }

    public void setNumpaginas(int numpaginas) {
        this.numpaginas = numpaginas;
    }
    
    public void mostrarDatos(){
        System.out.println("El libro con ISBN "+ISBN+", creado por el autor "+autor+" tiene "+numpaginas+" paginas.");
    }
}
