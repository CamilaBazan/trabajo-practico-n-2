public class Principal {
    
    public static void main(String[] args) {
        
        Libro lib1 = new Libro(978,"El Principito","Antoine De Saint-Exupery",110);
        Libro lib2 = new Libro(987,"Hamlet","William Shakespeare",188);
        
        lib1.mostrarDatos();
        lib2.mostrarDatos();
      
    }

}